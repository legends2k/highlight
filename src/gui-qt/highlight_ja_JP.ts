<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP" sourcelanguage="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <source>Themes or indent schemes not found.
Check installation.</source>
        <translation type="obsolete">Farb- oder Formatierungsvorlagen nicht gefunden.
Installation prüfen.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="140"/>
        <location filename="mainwindow.cpp" line="155"/>
        <location filename="mainwindow.cpp" line="171"/>
        <source>Initialization error</source>
        <translation>初期化エラー</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="141"/>
        <source>Could not read a colour theme: </source>
        <translation>カラーテーマが読み込めません: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="145"/>
        <source>light</source>
        <translation>ライト</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="146"/>
        <source>dark</source>
        <translation>ダーク</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="147"/>
        <source>B16 light</source>
        <translation>B16 ライト</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="148"/>
        <source>B16 dark</source>
        <translation>B16 ダーク</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="151"/>
        <source>Please select a Server</source>
        <translation>サーバを選択してください</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="156"/>
        <source>Could not find LSP profiles. Check installation.</source>
        <translation>LSPプロファイルが見つかりません。インストール作業を客員してください。</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="172"/>
        <source>Could not find syntax definitions. Check installation.</source>
        <translation>Syntax定義が見つかりません。
インストール作業を確認してください。</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="296"/>
        <source>NTFS Short Names</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="296"/>
        <source>NTFS short names may be disabled on your volumes. Highlight can not read input with multibyte file names if no short name is available. This information will no longer bother you.</source>
        <translation>NTFSのショートファイル名機能が無効化されているかもしれません.ショートファイル名機能が有効でない場合、Hightlightはマルチバイト文字のファイル名の入力ファイルを読み込めません.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <source>Always at your service</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="343"/>
        <source>Select one or more files to open</source>
        <translation>オープンする1つ以上のファイルの選択</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="380"/>
        <source>Select destination directory</source>
        <translation>出力ディレクトリの選択</translation>
    </message>
    <message>
        <source>Tags file error</source>
        <translation type="obsolete">Tags Dateifehler</translation>
    </message>
    <message>
        <source>Could not read tags information in &quot;%1&quot;</source>
        <translation type="obsolete">Konnte Tags-Informationen in %1 nicht lesen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1130"/>
        <location filename="mainwindow.cpp" line="1161"/>
        <source>Output error</source>
        <translation>出力エラー</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1130"/>
        <source>Output directory does not exist!</source>
        <translation>出力ディレクトリが存在しません!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1161"/>
        <source>You must define a style output file!</source>
        <translation>スタイル出力ファイルを指定してください!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1213"/>
        <source>Processing %1 (%2/%3)</source>
        <translation>処理中 %1 (%2/%3)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1223"/>
        <source>Language definition error</source>
        <translation>言語定義エラー</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1224"/>
        <source>Invalid regular expression in %1:
%2</source>
        <translation>無効な正規表現 in %1:
%2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1228"/>
        <source>Unknown syntax</source>
        <translation>不明なSyntax</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1228"/>
        <source>Could not convert %1</source>
        <translation>%1 を変換できません</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1231"/>
        <source>Lua error</source>
        <translation>Lua エラー</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1231"/>
        <source>Could not convert %1:
Lua Syntax error: %2</source>
        <translation>%1 を変換できません:
Lua Syntaxエラー: %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1406"/>
        <source>Converted %1 files in %2 ms</source>
        <translation>変換完了 %1 ファイル in %2 ms</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1610"/>
        <source>Conversion of &quot;%1&quot; not possible.</source>
        <translation>&quot;%1&quot;の変換が出来ません.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1610"/>
        <location filename="mainwindow.cpp" line="1758"/>
        <location filename="mainwindow.cpp" line="1813"/>
        <source>clipboard data</source>
        <translation>クリップボード データ</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1689"/>
        <source>%1 options</source>
        <translation>%1 オプション</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1730"/>
        <location filename="mainwindow.cpp" line="1741"/>
        <source>(user script)</source>
        <translation>(ユーザ　スクリプト)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1757"/>
        <source>Preview (%1):</source>
        <translation>プレビュー (%1):</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1760"/>
        <source>Current syntax: %1 %2</source>
        <translation>現在のSyntax: %1 %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1761"/>
        <source>Current theme: %1 %2</source>
        <translation>現在のテーマ: %1 %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1764"/>
        <source>Contrast: %1 %2</source>
        <translation>コントラスト: %1 %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2216"/>
        <source>Language server initialization successful</source>
        <translation>Language Serverの初期化に成功</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2221"/>
        <source>Language server connection failed</source>
        <translation>Language serverの接続に失敗 </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2223"/>
        <source>Language server initialization failed</source>
        <translation>Language Serverの初期化に失敗</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2251"/>
        <source>Select workspace directory</source>
        <translation>作業ディレクトリの選択</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2255"/>
        <source>Choose the Language Server executable</source>
        <translation>Language Server実行ファイルの選択</translation>
    </message>
    <message>
        <source>Current syntax: %1</source>
        <translation type="vanished">Aktuelle Syntax: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1813"/>
        <source>Preview of &quot;%1&quot; not possible.</source>
        <translation>&quot;%1&quot;のプレビューが出来ません.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2063"/>
        <source>Some plug-in effects may not be visible in the preview.</source>
        <translation>プレビューではプラグ-インの効果が確認できないものがあります.</translation>
    </message>
    <message>
        <source>Choose a ctags file</source>
        <translation type="obsolete">Wählen Sie eine ctags Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1857"/>
        <location filename="mainwindow.cpp" line="1861"/>
        <location filename="mainwindow.cpp" line="1865"/>
        <location filename="mainwindow.cpp" line="1870"/>
        <source>Choose a style include file</source>
        <translation>スタイルインクルードファイルの選択</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1875"/>
        <source>About providing translations</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1876"/>
        <source>The GUI was developed using the Qt toolkit, and translations may be provided using the tools Qt Linguist and lrelease.
The highlight.ts file for Linguist resides in the src/gui-qt subdirectory.
The qm file generated by lrelease has to be saved in gui-files/l10n.

Please send a note to as (at) andre-simon (dot) de if you have issues during translating or if you have finished or updated a translation.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1934"/>
        <source>Select one or more plug-ins</source>
        <translation>1つ以上のプラグ-インを選択</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1945"/>
        <source>Select one or more syntax or theme scripts</source>
        <translation>1つ以上の構文 or テーマ スクリプトの選択</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2005"/>
        <source>Choose a plug-in input file</source>
        <translation>プラグ-イン入力ファイルの選択</translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="mainwindow.ui" line="180"/>
        <source>Choose the source code files you want to convert.</source>
        <translation>変換するソースコードファイルの選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="186"/>
        <source>Choose input files</source>
        <translation>入力ファイルの選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="212"/>
        <source>List of input files.</source>
        <translation>入力ファイルのリスト</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="236"/>
        <source>Remove the selected input files.</source>
        <translation>選択した入力ファイルの削除</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="239"/>
        <location filename="mainwindow.ui" line="582"/>
        <location filename="mainwindow.ui" line="718"/>
        <source>Clear selection</source>
        <translation>選択したものをクリア</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="246"/>
        <source>Remove all input files.</source>
        <translation>すべての入力ファイルの削除</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="249"/>
        <location filename="mainwindow.ui" line="592"/>
        <location filename="mainwindow.ui" line="728"/>
        <source>Clear all</source>
        <translation>すべてをクリア</translation>
    </message>
    <message>
        <source>Output destination</source>
        <translation type="obsolete">Zielverzeichnis</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="288"/>
        <source>Output directory</source>
        <translation>出力ディレクトリ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="307"/>
        <source>Select the output directory.</source>
        <translation>出力ディレクトリの選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="310"/>
        <location filename="mainwindow.ui" line="669"/>
        <location filename="mainwindow.ui" line="819"/>
        <location filename="mainwindow.ui" line="884"/>
        <location filename="mainwindow.ui" line="1509"/>
        <location filename="mainwindow.ui" line="1778"/>
        <location filename="mainwindow.ui" line="1886"/>
        <location filename="mainwindow.ui" line="2181"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="345"/>
        <source>Save output in the input file directories.</source>
        <translation>入力ファイルディレクトリに出力</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="348"/>
        <source>Write to source directories</source>
        <translation>ソースディレクトリに書き込む</translation>
    </message>
    <message>
        <source>Output options</source>
        <translation type="obsolete">Ausgabeoptionen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="964"/>
        <source>General</source>
        <translation>一般</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="984"/>
        <source>Output for&amp;mat:</source>
        <oldsource>Output format:</oldsource>
        <translation>出力フォーマット:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1006"/>
        <source>Choose an output format.</source>
        <translation>出力フォーマットの選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1010"/>
        <source>HTML</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1015"/>
        <source>XHTML</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1020"/>
        <source>LaTeX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1025"/>
        <source>TeX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1030"/>
        <source>RTF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1035"/>
        <source>ODT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1040"/>
        <source>SVG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1050"/>
        <source>ANSI ESC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1055"/>
        <source>XTerm256 ESC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1060"/>
        <source>Truecolor ESC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1079"/>
        <source>Add line numbers to the output.</source>
        <translation>出力に行番号を付加</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1082"/>
        <source>Add line numbers</source>
        <translation>行番号を付加</translation>
    </message>
    <message>
        <source>Set line numbering start</source>
        <translation type="vanished">Lege den Beginn der Nummerierung fest</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1121"/>
        <source>Select the line number width.</source>
        <translation>行番号の幅の選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1142"/>
        <source>Fill leading space of line numbers with zeroes.</source>
        <translation>行番号の後ろを0で埋める</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1145"/>
        <source>Pad with zeroes</source>
        <translation>行番号を0でパディング</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1158"/>
        <source>Generate output without document header and footer.</source>
        <translation>ドキュメントのヘッダーとフッターを付加せずに出力</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1161"/>
        <source>Omit header and footer</source>
        <translation>ヘッダーとフッターを省略</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1168"/>
        <source>Output any plug-in text injections even if document header and footer is omitted.</source>
        <translation>ヘッダーとフッターを付加しない場合でもプラグ-インによる情報は出力する.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1171"/>
        <source>Keep Plug-In injections</source>
        <translation>プラグ-インによる挿入部分を維持</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1178"/>
        <source>Test if input data is not binary.
Removes Unicode BOM mark.</source>
        <translation>入力データがバイナリでないかを確認する.
Unicode BOM markを削除する.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1182"/>
        <source>Validate input data</source>
        <translation>入力データの正当性チェック</translation>
    </message>
    <message>
        <source>Set the output file ancoding.</source>
        <translation type="vanished">Setzt das Encoding.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2019"/>
        <source>I&amp;mage width:</source>
        <translation>イメージ(&amp;I)幅:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2067"/>
        <source>Height:</source>
        <oldsource>Image height:</oldsource>
        <translation>高さ:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2328"/>
        <source>Reformat and indent your code.
This feature is enabled for C, C++, C# and Java code.</source>
        <translation>コードを再フォーマット/インデントします.
この機能はC, C++, C#, Javaで利用可能です.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2662"/>
        <source>&amp;Plug-Ins</source>
        <translation>&amp;Plug-Ins</translation>
    </message>
    <message>
        <source>Encoding:</source>
        <translation type="obsolete">Encoding:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1211"/>
        <source>Select or define the encoding.
The result has to match the input file encoding.</source>
        <translation>エンコーディングの定義を選択.
入力ファイルのエンコーディングと一致しなければならない.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1222"/>
        <source>ISO-8859-1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1227"/>
        <source>ISO-8859-2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1232"/>
        <source>ISO-8859-3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1237"/>
        <source>ISO-8859-4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1242"/>
        <source>ISO-8859-5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1247"/>
        <source>ISO-8859-6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1252"/>
        <source>ISO-8859-7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1257"/>
        <source>ISO-8859-8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1262"/>
        <source>ISO-8859-9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1267"/>
        <source>ISO-8859-10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1272"/>
        <source>ISO-8859-11</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1277"/>
        <source>ISO-8859-12</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1282"/>
        <source>ISO-8859-13</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1287"/>
        <source>ISO-8859-14</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1292"/>
        <source>ISO-8859-15</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1297"/>
        <source>UTF-8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1321"/>
        <source>Output specific</source>
        <translation>出力指定</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1554"/>
        <source>Add an anchor to each line.</source>
        <translation>それぞれの行にアンカーを付加</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1618"/>
        <source>Add HTML MIME Type when copying code to the clipboard</source>
        <translation>クリップボードへコードをコピーする時HTML MIME Typeを付加</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1621"/>
        <source>Copy with MIME type</source>
        <translation>MIME typeを含めてコピー</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1659"/>
        <source>Adapt output for the Babel package</source>
        <translation>Babel パッケージに適合したコードを出力</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1669"/>
        <source>Adapt output for the Beamer package</source>
        <translation>Beamer パッケージに適合したコードを出力</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1672"/>
        <source>Add Beamer compatibility</source>
        <translation>Beamer互換のコードを付加</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1923"/>
        <source>Set page color attribute to background color.</source>
        <translation>ページ色属性を背景色にセット</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2536"/>
        <source>DejaVu Sans Mono</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2667"/>
        <source>&amp;File access trace (W32)</source>
        <translation></translation>
    </message>
    <message>
        <source>HTML options</source>
        <translation type="obsolete">HTML Optionen</translation>
    </message>
    <message>
        <source>Style</source>
        <translation type="obsolete">Stylesheets</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1370"/>
        <location filename="mainwindow.ui" line="1696"/>
        <location filename="mainwindow.ui" line="1804"/>
        <location filename="mainwindow.ui" line="2099"/>
        <source>Include the style information in each output file.</source>
        <translation>それぞれの出力ファイルにおけるスタイル情報を含む.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1373"/>
        <location filename="mainwindow.ui" line="2102"/>
        <source>Embed style (CSS)</source>
        <translation>埋め込みスタイル (CSS)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1386"/>
        <source>Add CSS information to each tag (do not use CSS class definitions).</source>
        <translation>CSS 情報をそれぞれのタグに追加 (CSSクラス定義を使ってはならない)..</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1389"/>
        <source>Inline CSS</source>
        <translation>インライン CSS</translation>
    </message>
    <message>
        <source>Style file:</source>
        <translation type="obsolete">Stylesheet-Datei:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1449"/>
        <location filename="mainwindow.ui" line="1832"/>
        <location filename="mainwindow.ui" line="2127"/>
        <source>Name of the referenced style file.</source>
        <translation>参照したスタイルファイル名</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1452"/>
        <location filename="mainwindow.ui" line="2130"/>
        <source>highlight.css</source>
        <translation></translation>
    </message>
    <message>
        <source>Style include file:</source>
        <translation type="obsolete">Eingabe-Stylesheet:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1493"/>
        <location filename="mainwindow.ui" line="2165"/>
        <source>Path of the CSS include file.</source>
        <translation>CSSインクルードファイルのパス</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1506"/>
        <source>Select a CSS include file.</source>
        <translation>CSSインクルードファイルの選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1403"/>
        <source>CSS class prefix:</source>
        <translation>CSS class プレフィックス:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="743"/>
        <source>Shows description of a selected script.</source>
        <translation>選択したスクリプトの記述を表示</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1416"/>
        <source>Add a CSS class name prefix to avoid namespace clashes.</source>
        <translation>名前空間(namespace)の衝突を避けるため CSS クラス名のプレフィックスを追加.</translation>
    </message>
    <message>
        <source>Tags and index files</source>
        <translation type="obsolete">Tags- und Indexdateien</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1591"/>
        <source>Generate an index file with hyperlinks to all outputted files.</source>
        <translation>すべての出力ファイルへリンクを持つインデックスファイルを生成</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1594"/>
        <source>Generate index file</source>
        <translation>インデックスファイルを生成</translation>
    </message>
    <message>
        <source>Read a ctags file and add the included metainformation as tooltips.
See ctags.sf.net for details.</source>
        <translation type="obsolete">Lese eine ctags-Datei und füge die Metainformationen als Tooltips hinzu.
Siehe ctags.sf.net um mehr darüber zu erfahren.</translation>
    </message>
    <message>
        <source>Ctags file:</source>
        <translation type="obsolete">Ctags Datei:</translation>
    </message>
    <message>
        <source>Path of the ctags file.</source>
        <translation type="obsolete">Pfad der ctags-Datei.</translation>
    </message>
    <message>
        <source>Choose a ctags file.</source>
        <translation type="obsolete">Wähle eine ctags Datei.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1535"/>
        <source>Misc</source>
        <translation>Misc</translation>
    </message>
    <message>
        <source>Add an achor to each line.</source>
        <translation type="vanished">Füge Anker zu jeder Zeile hinzu.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1557"/>
        <source>Add line anchors</source>
        <translation>行アンカーを付加</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1564"/>
        <source>Add the filename as prefix to the anchors.</source>
        <translation>アンカーにプレフィックスとしてファイル名を付加</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1567"/>
        <source>Include file name in anchor</source>
        <translation>アンカーにファイル名を付加</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1574"/>
        <source>Output the lines within an ordered list.</source>
        <translation></translation>
    </message>
    <message>
        <source>Ordered list</source>
        <translation type="obsolete">Sortierte Liste</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1601"/>
        <source>Add &amp;lt;pre&amp;gt; tags to the output, if the flag &quot;No document header and footer&quot; is selected.</source>
        <translation></translation>
    </message>
    <message>
        <source>Enclose in &lt;pre&gt;</source>
        <translation type="obsolete">Schließe in &amp;lt;pre&amp;gt; ein</translation>
    </message>
    <message>
        <source>LaTeX options</source>
        <translation type="obsolete">LaTeX Optionen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1649"/>
        <source>Replace quotes by dq sequences.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1652"/>
        <source>Escape quotes</source>
        <translation></translation>
    </message>
    <message>
        <source>Make output Babel compatible.</source>
        <translation type="vanished">Erzeuge Code, der mit Babel verträglich ist.</translation>
    </message>
    <message>
        <source>Babel compatibility</source>
        <translation type="obsolete">Babel Kompatibilität</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1679"/>
        <source>Replace default symbols (brackets, tilde) by nice redefinitions.</source>
        <translation>デフォルトシンボル (brackets, tilde) を再定義により置き換える</translation>
    </message>
    <message>
        <source>Pretty symbols</source>
        <translation type="obsolete">Schönere Symbole</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1699"/>
        <source>Embed style (defs)</source>
        <translation>埋め込みスタイル (defs)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1724"/>
        <source>Name of the referenced  style file.</source>
        <translation>参照されるスタイルファイルの名前</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1727"/>
        <location filename="mainwindow.ui" line="1835"/>
        <source>highlight.sty</source>
        <translation></translation>
    </message>
    <message>
        <source>Style include:</source>
        <translation type="obsolete">Eingabe-Stylesheet:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1204"/>
        <source>Set encoding:</source>
        <translation>文字エンコーディング:</translation>
    </message>
    <message>
        <source>Read ctags file:</source>
        <translation type="obsolete">Lese ctags-Datei:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1577"/>
        <source>Output as ordered list</source>
        <translation>整列したリストとして出力</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1604"/>
        <source>Enclose in pre tags</source>
        <translation>preタグで囲む</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1662"/>
        <source>Add Babel compatibility</source>
        <translation>Babel互換性を付加</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1682"/>
        <source>Add pretty symbols</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1430"/>
        <location filename="mainwindow.ui" line="1708"/>
        <location filename="mainwindow.ui" line="1816"/>
        <location filename="mainwindow.ui" line="2111"/>
        <source>Stylesheet file:</source>
        <translation>スタイルシート ファイル:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="273"/>
        <source>Output destination:</source>
        <translation>出力先:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="329"/>
        <source>Browse output directory</source>
        <translation>出力ディレクトリを開く</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="396"/>
        <source>Copy highlighted code of the selected file into the clipboard.</source>
        <translation>選択したファイルのハイライトしたコードをクリップボードにコピー</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="481"/>
        <source>Paste code from clipboard and copy the output back in one step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="487"/>
        <source>Paste, Convert and Copy</source>
        <translation>ペースト、変換後コピー</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="513"/>
        <source>Only output the selected lines of the preview.</source>
        <translation>プレビューの選択した行のみを出力</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="516"/>
        <source>Output selected lines only</source>
        <translation>選択した行のみを出力</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="613"/>
        <source>Shows description of a selected plug-in script.</source>
        <translation>選択したプラグ-イン スクリプトの記述を表示</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="683"/>
        <source>Scripts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="689"/>
        <source>Choose a custom syntax or theme script.</source>
        <translation>カスタム構文もしくはテーマスクリプトを選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="692"/>
        <source>Add custom script to pool</source>
        <translation>カスタムスクリプトをプールに追加</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="706"/>
        <source>You can check one file of each type (syntax and theme) to override the default behaviour. If checked, the scripts will be watched for changes.</source>
        <oldsource>You can check one file of each type (syntax and theme) to override the default behaviour.</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="715"/>
        <source>Remove the selected scripts.</source>
        <translation>選択したスクリプトを削除</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="725"/>
        <source>Remove all scripts.</source>
        <translation>すべてのスクリプトを削除</translation>
    </message>
    <message>
        <source>Shows decription of a selected script.</source>
        <translation type="vanished">Zeigt die Beschreibung des gewählten Skripts an.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="746"/>
        <source>Script description</source>
        <translation>スクリプトの説明</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="757"/>
        <source>Configure the LSP Client</source>
        <translation>LSP-Client のコンフィグレーション</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="764"/>
        <source>LSP</source>
        <translation>LSP</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="772"/>
        <source>Language Server:</source>
        <translation>Language Server:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="779"/>
        <source>Select a Language Server profile.</source>
        <translation>Language Serverのプロファイル選択.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="796"/>
        <source>Executable:</source>
        <translation>実行ファイル:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="803"/>
        <source>Path of the server executable.</source>
        <translation>サーバー実行ファイルのパス.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="816"/>
        <source>Select the server executable.</source>
        <translation>サーバー実行ファイルの選択.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="830"/>
        <source>Print LSP messages to standard error</source>
        <translation>LSPメッセージを標準出力に出力</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="833"/>
        <source>Output Logs</source>
        <translation>ログ出力</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="840"/>
        <source>Start an initialization request.</source>
        <translation>初期化リクエストの開始.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="843"/>
        <source>Check Capabilities</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="861"/>
        <source>Workspace:</source>
        <translation>ワークスペース:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="868"/>
        <source>The workspace directory.</source>
        <translation>作業ディレクトリ.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="881"/>
        <source>Select the workspace directory of your project.</source>
        <translation>プロジェクトのワークスペースディレクトリの選択.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="893"/>
        <source>Run hover requests to add tooltips to your output (HTML only).</source>
        <translation>出力にツールチップを追加するためのHoverリクエストの実行 (HTML only).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="896"/>
        <source>Hover</source>
        <translation>Hover</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="906"/>
        <source>Improve token recognition with semantic highlighting.</source>
        <translation>Semantic Highlightingによるトークン認識の改善.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="909"/>
        <source>Semantic Highlighting</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="919"/>
        <source>Show syntax errors and messages (depends on output format).</source>
        <translation>Syntax Errorとメッセージの表示 (出力フォーマットに依存).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="922"/>
        <source>Show Syntax Errors</source>
        <translation>Syntax Errorの表示</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1102"/>
        <source>Set line numbering start.</source>
        <translation>開始行番号を設定</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1189"/>
        <source>Generate output without version information comment.</source>
        <translation>バージョン情報コメントなしで出力を生成</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1192"/>
        <source>Omit version info comment</source>
        <translation>バージョン情報コメントを省略</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1201"/>
        <source>Set the output file encoding.</source>
        <translation>出力ファイルの文字エンコーディングを設定</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1463"/>
        <location filename="mainwindow.ui" line="1738"/>
        <location filename="mainwindow.ui" line="1846"/>
        <location filename="mainwindow.ui" line="2141"/>
        <source>Stylesheet include file:</source>
        <translation>スタイルシート インクルードファイル:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1547"/>
        <source>Line numbering options:</source>
        <translation>行番号オプション:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="17"/>
        <source>Highlight</source>
        <translation>Highlight</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="174"/>
        <source>Files</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="435"/>
        <source>Paste clipboard content into the preview window.</source>
        <translation>クリップボードの内容をプレビューウィンドウにペースト</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="438"/>
        <source>Paste from clipboard (%1)</source>
        <oldsource>Paste from clipboard</oldsource>
        <translation>クリップボードからペースト (%1)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="455"/>
        <source>Copy highlighted code into the clipboard.</source>
        <translation>ハイライトしたコードをクリップボードにコピー</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="458"/>
        <source>Copy preview to clipboard (%1)</source>
        <oldsource>Copy preview to clipboard</oldsource>
        <translation>プレビューをクリップボードにコピー (%1)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="419"/>
        <source>Select syntax:</source>
        <translation>Syntax の選択:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="426"/>
        <source>Select the correct syntax of the code snippet.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="541"/>
        <source>Plug-ins</source>
        <translation>Plug-Ins</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="550"/>
        <source>Add plug-in to pool</source>
        <translation>プラグ-インをプールに追加</translation>
    </message>
    <message>
        <source>List of plug-ins.</source>
        <translation type="obsolete">Sammlung von Plug-Ins.</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Remove the selected plug-ins.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">Entferne die selektierten Plug-Ins</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Remove all plug-ins.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">Entferne alle Plug-Ins</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Use the selected plugin.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">Bneutze das ausgewählte Plug-In</translation>
    </message>
    <message>
        <source>Enable selected plug-in script</source>
        <translation type="obsolete">Aktiviere das selektierte Plug-In Skript</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1045"/>
        <source>BBCode</source>
        <translation>BBCode</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1362"/>
        <source>Stylesheets</source>
        <translation>Stylesheets</translation>
    </message>
    <message>
        <source>Include:</source>
        <translation type="vanished">Eingabedatei:</translation>
    </message>
    <message>
        <source>Index/ctags</source>
        <translation type="obsolete">Index/ctags</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1762"/>
        <location filename="mainwindow.ui" line="1870"/>
        <source>Path of the style include file.</source>
        <translation>スタイルインクルードファイルのパス.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1775"/>
        <location filename="mainwindow.ui" line="1883"/>
        <location filename="mainwindow.ui" line="2178"/>
        <source>Select a style include file.</source>
        <translation>スタイルインクルードファイルの選択.</translation>
    </message>
    <message>
        <source>TeX options</source>
        <translation type="obsolete">TeX Optionen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1807"/>
        <source>Embed style (macros)</source>
        <translation>埋め込みスタイル (macros)</translation>
    </message>
    <message>
        <source>RTF options</source>
        <translation type="obsolete">RTF Optionen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1912"/>
        <source>Add character stylesheets with formatting information.
You can select the stylesheets in your word processor to reformat additional text.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1916"/>
        <source>Add character styles</source>
        <translation>文字スタイルを付加</translation>
    </message>
    <message>
        <source>Set page color attribute to background color</source>
        <translation type="vanished">Setze die Hintergrundfarbe als Seitenfarbe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1926"/>
        <source>Set page color</source>
        <translation>ページ色を設定</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1935"/>
        <source>Page size:</source>
        <translation>ページサイズ:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1948"/>
        <source>Select a page size.</source>
        <translation>ページサイズを選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1955"/>
        <source>A3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1960"/>
        <source>A4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1965"/>
        <source>A5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1970"/>
        <source>B4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1975"/>
        <source>B5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1980"/>
        <source>B6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1985"/>
        <source>Letter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1990"/>
        <source>Legal</source>
        <translation></translation>
    </message>
    <message>
        <source>SVG options</source>
        <translation type="obsolete">SVG Optionen</translation>
    </message>
    <message>
        <source>Image size:</source>
        <translation type="vanished">Bildgröße:</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation type="vanished">Breite:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2047"/>
        <source>Enter the SVG width (may contain units).</source>
        <translation>SVG 幅の入力.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2083"/>
        <source>Enter the SVG height (may contain units).</source>
        <translation>SVG 高さの入力.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2207"/>
        <source>No options defined.</source>
        <translation>オプションが定義されてません</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2232"/>
        <source>Formatting</source>
        <translation>フォーマット</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2261"/>
        <source>Color theme:</source>
        <translation>カラーテーマ:</translation>
    </message>
    <message>
        <source>Toggle classic theme or Base16 theme selection.</source>
        <translation type="vanished">Wechsle zwischen den klassischen und den Base16 Themes.</translation>
    </message>
    <message>
        <source>Base16</source>
        <translation type="vanished">Base16</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2308"/>
        <source>Select a colour theme.</source>
        <translation>カラーテーマの選択.</translation>
    </message>
    <message>
        <source>Reformat and indent your code.
This feature is enabled tor C, C++, C# and Java code.</source>
        <translation type="vanished">Formatiere den Eingabecode.
Diese Funktion kann auf C, C++, C# und Java-Code angewandt werden.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2332"/>
        <source>Reformat:</source>
        <translation>再フォーマット:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2351"/>
        <source>Choose a formatting scheme.</source>
        <translation>フォーマットスキームの選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2365"/>
        <source>Change the keyword case.</source>
        <translation>キーワードの大文字、小文字の変更</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2368"/>
        <source>Keyword case:</source>
        <translation>キーワードの小文字/大文字:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2387"/>
        <source>Select a keyword case.</source>
        <translation>キーワードの大文字、小文字の選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2394"/>
        <source>UPPER</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2399"/>
        <source>lower</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2404"/>
        <source>Capitalize</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2422"/>
        <source>Tab width:</source>
        <translation>タブ幅</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2438"/>
        <source>Enter the number of spaces which replace a tab.
Set the width to 0 to keep tabs.</source>
        <translation>タブを空白に変換する際の文字数を入力.
0を入力するとタブの置き換えは行わない</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2453"/>
        <source>Enable line wrapping.</source>
        <translation>行の折り返し(Line Wrapping)を有効化.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2456"/>
        <source>Line wrapping</source>
        <translation>行の折り返し(Line Wrapping)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2469"/>
        <source>Enter the maximum line length.</source>
        <translation>最大行数の入力.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2488"/>
        <source>Indent statements and function parameters after wrapping.</source>
        <translation>行の折り返し(Line Wrapping)後のステートメント、関数パラメータのインデント</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2491"/>
        <source>Intelligent wrapping</source>
        <translation>Intelligent Wrapping</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2512"/>
        <source>Font na&amp;me:</source>
        <oldsource>Font name:</oldsource>
        <translation>フォント名:</translation>
    </message>
    <message>
        <source>Select or enter the font name.</source>
        <translation type="obsolete">Wähle oder gib die Schriftart an.</translation>
    </message>
    <message>
        <source>Font size:</source>
        <translation type="vanished">Schriftgröße:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2573"/>
        <source>Enter the font size.</source>
        <translation>フォントサイズを入力</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2646"/>
        <source>&amp;Visit andre-simon.de</source>
        <oldsource>Visit andre-simon.de</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2657"/>
        <source>&amp;Dock floating panels</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <source>Preview</source>
        <translation>プレビュー</translation>
    </message>
    <message>
        <source>Visit website</source>
        <translation type="obsolete">Website aufrufen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="383"/>
        <source>Start the conversion of your input files.</source>
        <translation>入力ファイルの変換開始</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="386"/>
        <source>Convert files</source>
        <translation>ファイルを変換</translation>
    </message>
    <message>
        <source>Copy highlighted code of the seleted file into the clipboard.</source>
        <translation type="vanished">Kopiere den Inhalt der ausgewählten Datei mit Highlighting in die Zwischenablage.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="411"/>
        <source>Clipboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="399"/>
        <source>Copy file to clipboard</source>
        <translation>ファイルをクリップボードにコピー</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="547"/>
        <source>Choose a plug-in script.</source>
        <translation>Plug-In スクリプトを選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="561"/>
        <source>List of plug-ins. Toggle checkbox to enable the scripts. The preview window may not display all plug-in effects.</source>
        <oldsource>List of plug-ins. Toggle checkbox to enable the scripts.</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="579"/>
        <source>Remove the selected plug-ins.</source>
        <translation>選択したPlug-Inを削除</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="589"/>
        <source>Remove all plug-ins.</source>
        <translation>すべてのPlug-Inを削除</translation>
    </message>
    <message>
        <source>Use the selected plugin.</source>
        <translation type="obsolete">Benutze das ausgewählte Plug-In.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <source>Output progress:</source>
        <translation>出力進捗:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="86"/>
        <source>&amp;File</source>
        <translation>ファイル(&amp;F)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>&amp;Help</source>
        <translation>ヘルプ(&amp;H)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="109"/>
        <source>Wi&amp;ndows</source>
        <oldsource>&amp;Windows</oldsource>
        <translation>ウィンドウ(&amp;N)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="129"/>
        <source>Highlighting options</source>
        <oldsource>H&amp;ighlighting options</oldsource>
        <translation></translation>
    </message>
    <message>
        <source>You can apply system shortcuts for copy and paste.</source>
        <translation type="vanished">Sie können die System-Tastenkombinationen zum Kopieren und Einfügen verwenden.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="601"/>
        <location filename="mainwindow.ui" line="737"/>
        <source>Description</source>
        <translation>説明</translation>
    </message>
    <message>
        <source>Shows decription of a selected plug-in script.</source>
        <translation type="vanished">Zeigt Beschreibung des ausgewählten Plug-In Skripts.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="616"/>
        <source>Plug-In description</source>
        <translation>Plug-Inの説明</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="644"/>
        <source>Plug-in parameter</source>
        <oldsource>Plug-in input file</oldsource>
        <translation>Plug-In パラメータ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="653"/>
        <source>Optional plug-in parameter, this may be a path to a plug-in input file</source>
        <oldsource>Optional path to a plug-in input file</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="666"/>
        <source>Select the plug-in input file.</source>
        <translation>Plug-in 入力ファイルの選択</translation>
    </message>
    <message>
        <source>Replace spaces by nbsp entities.</source>
        <oldsource>Replace spaces by &amp;nbsp; entities.</oldsource>
        <translation type="obsolete">Ersetze Leerzeichen durch nbsp-Codes.</translation>
    </message>
    <message>
        <source>Use non breaking spaces</source>
        <translation type="obsolete">Benutze geschützte Leerzeichen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2498"/>
        <source>Do not add line numbering to lines which were automatically wrapped.</source>
        <translation>自動で折り返し(Line Wrapping)した行に行番号を付加しない</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2501"/>
        <source>Omit line numbers of wrapped lines</source>
        <translation>折り返し(Line Wrapping)した行の行番号を省略</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2533"/>
        <source>Select or enter the font name. HTML supports a list of fonts, separated with comma.</source>
        <translation>フォント名の選択. HTMLがサポートするフォントのリストをカンマで区切って並べる.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2551"/>
        <source>Font si&amp;ze:</source>
        <translation>フォントサイズ:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2588"/>
        <source>&amp;Open files</source>
        <translation>ファイルを開く(&amp;O)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2593"/>
        <source>&amp;Exit</source>
        <translation>終了(&amp;E)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2598"/>
        <source>&amp;Load</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2603"/>
        <source>&amp;Save</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2608"/>
        <source>Load &amp;default project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2613"/>
        <source>&amp;Readme</source>
        <oldsource>Readme</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2618"/>
        <source>&amp;Tips</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2626"/>
        <source>&amp;Changelog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2631"/>
        <source>&amp;License</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2636"/>
        <source>&amp;About Highlight</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2641"/>
        <source>A&amp;bout translations</source>
        <oldsource>About &amp;translations</oldsource>
        <translation></translation>
    </message>
</context>
<context>
    <name>ShowTextFile</name>
    <message>
        <location filename="showtextfile.ui" line="17"/>
        <source>Show text</source>
        <translation>Textanzeige</translation>
    </message>
    <message>
        <location filename="showtextfile.ui" line="37"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="showtextfile.ui" line="71"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>io_report</name>
    <message>
        <location filename="io_report.ui" line="14"/>
        <source>Error summary</source>
        <translation></translation>
    </message>
    <message>
        <location filename="io_report.ui" line="24"/>
        <source>Input errors:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="io_report.ui" line="34"/>
        <source>Remove files above from input file list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="io_report.ui" line="48"/>
        <source>Output errors:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="io_report.ui" line="65"/>
        <source>Reformatting not possible:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="io_report.ui" line="82"/>
        <source>Failed syntax tests:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>syntax_chooser</name>
    <message>
        <location filename="syntax_chooser.ui" line="14"/>
        <source>Syntax selection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="32"/>
        <source>Select correct syntax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="50"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="74"/>
        <source>These entries are configured in filetypes.conf.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="81"/>
        <source>The selection will be remembered for this session.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="84"/>
        <source>Remember selection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="syntax_chooser.cpp" line="57"/>
        <source>The file extension %1 is assigned to multiple syntax definitions.
Select the correct one:</source>
        <translation></translation>
    </message>
</context>
</TS>
